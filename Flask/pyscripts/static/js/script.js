/*

classList.toggle("class-name");
*/
// Navigation
const navigation_div = document.getElementById("navigation");
const downArrow_span = document.querySelector(".close-icon");
const save_a = document.getElementById("save-icon");
const upload_a = document.getElementById("upload-icon");


// Color pickers
const redButton_btn = document.getElementById("button-red");
const blueButton_btn = document.getElementById("button-blue");
const greenButton_btn = document.getElementById("button-green");
const yellowButton_btn = document.getElementById("button-yellow");
const orangeButton_btn = document.getElementById("button-orange");
const whiteButton_btn = document.getElementById("button-white");

const resetButton_btn = document.getElementById("reset-button");

// Cube buttons
const cubeButtons_btn = document.querySelectorAll(".cube-buttons");

function pickedButton(color,click){
    
    switch (color){
       
        //User wins
        case "red":
            redButton_btn.style.opacity = 1;
            redButton_btn.classList.add("button-clicked");

            blueButton_btn.style.opacity = 0.5;
            blueButton_btn.classList.remove("button-clicked");
            greenButton_btn.style.opacity = 0.5;
            greenButton_btn.classList.remove("button-clicked");
            yellowButton_btn.style.opacity = 0.5;
            yellowButton_btn.classList.remove("button-clicked");
            orangeButton_btn.style.opacity = 0.5;
            orangeButton_btn.classList.remove("button-clicked");
            whiteButton_btn.style.opacity = 0.5;
            whiteButton_btn.classList.remove("button-clicked");
            break;
        
        case "blue":
            blueButton_btn.style.opacity = 1;
            blueButton_btn.classList.add("button-clicked");

            redButton_btn.style.opacity = 0.5;
            redButton_btn.classList.remove("button-clicked");
            greenButton_btn.style.opacity = 0.5;
            greenButton_btn.classList.remove("button-clicked");
            yellowButton_btn.style.opacity = 0.5;
            yellowButton_btn.classList.remove("button-clicked");
            orangeButton_btn.style.opacity = 0.5;
            orangeButton_btn.classList.remove("button-clicked");
            whiteButton_btn.style.opacity = 0.5;
            whiteButton_btn.classList.remove("button-clicked");
            break; 
        
        case "green":
            greenButton_btn.style.opacity = 1;
            greenButton_btn.classList.add("button-clicked");

            redButton_btn.style.opacity = 0.5;
            redButton_btn.classList.remove("button-clicked");
            blueButton_btn.style.opacity = 0.5;
            blueButton_btn.classList.remove("button-clicked");
            yellowButton_btn.style.opacity = 0.5;
            yellowButton_btn.classList.remove("button-clicked");
            orangeButton_btn.style.opacity = 0.5;
            orangeButton_btn.classList.remove("button-clicked");
            whiteButton_btn.style.opacity = 0.5;
            whiteButton_btn.classList.remove("button-clicked");
            break;

        case "yellow":
            yellowButton_btn.style.opacity = 1;
            yellowButton_btn.classList.add("button-clicked");

            redButton_btn.style.opacity = 0.5;
            redButton_btn.classList.remove("button-clicked");
            blueButton_btn.style.opacity = 0.5;
            blueButton_btn.classList.remove("button-clicked");
            greenButton_btn.style.opacity = 0.5;
            greenButton_btn.classList.remove("button-clicked");
            orangeButton_btn.style.opacity = 0.5;
            orangeButton_btn.classList.remove("button-clicked");
            whiteButton_btn.style.opacity = 0.5;
            whiteButton_btn.classList.remove("button-clicked");
            break;

        case "orange":
            orangeButton_btn.style.opacity = 1;
            orangeButton_btn.classList.add("button-clicked");

            redButton_btn.style.opacity = 0.5;
            redButton_btn.classList.remove("button-clicked");
            blueButton_btn.style.opacity = 0.5;
            blueButton_btn.classList.remove("button-clicked");
            yellowButton_btn.style.opacity = 0.5;
            yellowButton_btn.classList.remove("button-clicked");
            greenButton_btn.style.opacity = 0.5;
            greenButton_btn.classList.remove("button-clicked");
            whiteButton_btn.style.opacity = 0.5;
            whiteButton_btn.classList.remove("button-clicked");
            break;

        case "white":
            whiteButton_btn.style.opacity = 1;
            whiteButton_btn.classList.add("button-clicked");

            redButton_btn.style.opacity = 0.5;
            redButton_btn.classList.remove("button-clicked");
            blueButton_btn.style.opacity = 0.5;
            blueButton_btn.classList.remove("button-clicked");
            yellowButton_btn.style.opacity = 0.5;
            yellowButton_btn.classList.remove("button-clicked");
            orangeButton_btn.style.opacity = 0.5;
            orangeButton_btn.classList.remove("button-clicked");
            greenButton_btn.style.opacity = 0.5;
            greenButton_btn.classList.remove("button-clicked");
            break;                

    }
 

}




function main(){

    // COLOR PICKER BUTTONS EVENTS
    //******************
    //Red button clicked
    redButton_btn.addEventListener('click', () =>{
        console.log("Red button");
        pickedButton("red");
                               
    })
    
    blueButton_btn.addEventListener('click', () =>{
        console.log("Blue button");
        pickedButton("blue");
        
    })
    greenButton_btn.addEventListener('click', () =>{
        console.log("Green button");
        pickedButton("green");
        
    })
    yellowButton_btn.addEventListener('click', () =>{
        console.log("Yellow button");
        pickedButton("yellow");
        
    })
    orangeButton_btn.addEventListener('click', () =>{
        console.log("Orange button");
        pickedButton("orange");
        
    })
    whiteButton_btn.addEventListener('click', () =>{
        console.log("White button");
        pickedButton("white");
        
    })


    // Cube buttons
    cubeButtons_btn.forEach(el => el.addEventListener('click', () =>{
        /* for(let i=0; i<cubeButtons_btn.length;i++){
            console.log(cubeButtons_btn[i]);
        } */
        handler();        
    }))



    



    
    
    
    
    
    
    
    
    // SIDEBARD EVENTS
    //****************
    //Up arrow clicked
    downArrow_span.addEventListener('click', () => {
        console.log("Down arrow");
               
    })

    //Save button clicked
    save_a.addEventListener('click', () =>{
        console.log("Save");
        
    })

    upload_a.addEventListener('click', () => {
        console.log("Upload");
    })

    resetButton_btn.addEventListener('click', () =>{
        reset();
    })
    
}





function handler(){
   console.log("kliknuo si cube-button");
    
    
   for(var i=0;i<cubeButtons_btn.length;i++){
        cubeButtons_btn[i].style.backgroundColor = "red";
        cubeButtons_btn[i].style.opacity = 1;
    }
}

function reset(){
    for(var i=0;i<cubeButtons_btn.length;i++){
        cubeButtons_btn[i].style.backgroundColor = "";
        cubeButtons_btn[i].style.opacity = 0.5;
    }
}
main();


