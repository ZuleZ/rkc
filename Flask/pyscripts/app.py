import os, subprocess, signal
from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def index():
  return render_template('index.html')


#background process happening without any refreshing
@app.route('/background_process_test')
def background_process_test():
  global pid
  print ("Hello, start camera")
  p = subprocess.Popen(["python", "cubenew.py"])
  pid = p.pid
  p.wait() 

def get_pid():

    return str(subprocess.check_output(['grep', '-a', 'cubenew.py']).strip())

@app.route('/kill_cubenew')
def kill_cubenew():
    print("Kill attempt")
    global pid
    print(pid)
    if pid is not None:
      os.kill(pid, signal.SIGTERM)
      pid = None
    return "Hey"



# #background process happening without any refreshing
# @app.route('/background_process_test')
# def background_process_test():
#     print ("Hello, start camera")
#     return subprocess.call('python cubenew.py')
#     # return os.system('cubenew.py')

# def get_pid():

#     return str(subprocess.check_output(['grep', '-a', 'cubenew.py']).strip())

# @app.route('/kill_cubenew')
# def kill_cubenew():
#     print("Kill attempt")
#     pid =  get_pid()
#     print(pid)
#     os.kill(pid, signal.SIGTERM)

# @app.route()
# def kill():
#     return os.kill('cubenew.py')
 
#  return os.system('cubenew.py')

if __name__ == '__main__':
  app.run(debug=True)
