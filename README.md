# RKC
Python web app that allows you camera input of Rubik's cube.

When you put Rubik's cube in front of a camera, front side should be White(while the green midle should be pointing upwards)
Keys for input are 'F','B','R','L','U','D'. Front, Back, Right, Left, Up, Down.
After getting all of the colors, press ENTER.
You will get the solution if you had the correct inputs.
